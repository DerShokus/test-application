#ifndef TEST_APP_INCLUDE_TEST_TASK_H
#define TEST_APP_INCLUDE_TEST_TASK_H

#include <string>
#include <vector>
#include <initializer_list>
#include <iostream>

#include <dirent.h>

#include "TestFactory.hpp"

class Tasks
{
 private:
  const std::vector<std::string> dirs;
  std::vector<TestFactory::SharedTest> tests;

 public:
  Tasks(std::initializer_list<std::string> params) : dirs(params)
  {
    refresh();
  }

  void refresh()
  {
    DIR *dir_handle = opendir(dirs[0].c_str());
    dirent *dir_container;
    if(dir_handle != NULL)
      while ((dir_container = readdir(dir_handle)))
        if(dir_container->d_type == DT_REG)
        {
          try
          {
            TestFactory::SharedTest test = TestFactory::get_header(dir_container->d_name);
            if(test.get() != NULL)
            {
              std::cout << "add test: " << test.get()->name << std::endl;
              tests.push_back(test);
            }
          }
          catch(std::string err)
          {
            std::cout << "exception: " << err << std::endl;
          }
        }
  }
};

#endif
