#ifndef TEST_APP_INCLUDE_TEST_TEST_H
#define TEST_APP_INCLUDE_TEST_TEST_H

#include <unordered_map>

#include "Question.hpp"
#include "Answer.hpp"

template <class T_QUESTION, class T_ANSWER>
class Test
{
 protected:
  std::unordered_map<
      std::shared_ptr<T_QUESTION>, 
      std::shared_ptr<T_ANSWER>> answers;

 public:
  virtual size_t size() const=0;
  virtual Question question(const size_t index) const=0;
  virtual Answer answer(const size_t index) const=0;
  virtual bool correct(const std::shared_ptr<T_QUESTION>& question, const std::shared_ptr<T_ANSWER>& answer) const=0;
};

#endif
