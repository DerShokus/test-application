#ifndef TEST_APP_TEST_FACTIRY
#define TEST_APP_TEST_FACTIRY

#include <memory>
#include <cstddef>

#include "Header.hpp"


class TestFactory
{
 public:
  using SharedTest = std::shared_ptr<::TestHeader>;
  
  static SharedTest get_header(const std::string& path);
};

#endif

