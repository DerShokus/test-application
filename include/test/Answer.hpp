#ifndef TEST_APP_INCLUDE_TEST_ANSWER_H
#define TEST_APP_INCLUDE_TEST_ANSWER_H

#include "../md5/md5.h"


class Answer
{
 protected:
  MD5::hash answer_hash;
    
 public:
  virtual MD5::hash answer() const { return answer_hash; }
};


class TextAnswer : public Answer
{
 private:
  std::string wording;

 public:
  TextAnswer(const std::string& str) : wording(str) {} 
  std::string get_wording() const { return wording; }
};
#endif
