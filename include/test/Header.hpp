#ifndef TEST_APP_INCLUDE_TEST_HEADER_H
#define TEST_APP_INCLUDE_TEST_HEADER_H

#include <string>

#include "../md5/md5.h"

struct TestHeader
{
  std::string name;
  std::string description;

  virtual ~TestHeader() {}

  virtual MD5::hash hash() const
  {
    MD5 md5(name + description);
    return md5.result();
  }
};

#endif
