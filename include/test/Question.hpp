#ifndef TEST_APP_INCLUDE_TEST_QUESTION_H
#define TEST_APP_INCLUDE_TEST_QUESTION_H

#include "../md5/md5.h"


class Question
{
 protected:
  MD5::hash test_hash;
  MD5::hash question_hash;

 public:
  virtual MD5::hash test() const { return test_hash; }
  virtual MD5::hash question() const { return question_hash; }
};


class TextQuestion : public Question
{
 private:
  std::string wording;

 public:
  TextQuestion(std::string& str) : wording(str) {}

  std::string get_wording() const { return wording; }
};

#endif
