#include "JVocab/Test.hpp"
#include <fstream>
#include <iostream>
#include <memory>

#include "../include/test/TestFactory.hpp"
#include "../include/test/Task.hpp"

#include "TestChoose.hpp"
#include "SimpleConsoleUI.hpp"

void showusage()
{
  std::cout << "need argument - path to the test file" << std::endl;
}

int main(int argc, char *argv[])
{
  try {

  if(argc != 2)
  {
    std::cout << argc << std::endl;
    showusage();
    return 1;
  }

  JVocab::Test test;
  JVocab::Header header;

  std::vector<std::shared_ptr<Chooser::Task>> tasks;

  std::fstream fs;
  fs.open(argv[1]);
  fs >> test;
  fs.close();


  //std::cout << test;

 Chooser::Generator generator;
 tasks = generator.generate(test, Chooser::ChunkedGenerator());

  std::cout << "test size: " << test.size() << std::endl;
  std::cout << "tasks count: " << tasks.size() << std::endl;

  fs.open(argv[1]);
  fs >> header;
  fs.close();

  std::cout << header << std::endl;

  std::vector<JVocab::Test> tests = { test };
  SimpleConsole::UI ui; ui.start(tests);


  } catch(const char *str)
  {
    std::cout << "Exeption: " << str << std::endl;
  }

  return 0;
}
