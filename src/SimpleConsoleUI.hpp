#ifndef SIMPLE_CONSOLE_UI_H
#define SIMPLE_CONSOLE_UI_H

#include <vector>

#include "../include/test/Header.hpp"

#include "JVocab/Test.hpp"
#include "TestChoose.hpp"

namespace SimpleConsole
{
class View
{
};

class UI
{
 public:

  typedef std::vector<TestHeader> TestHeaders;

  void start(std::vector<JVocab::Test>& headers)
  {
    auto test = test_choose(headers);

    Chooser::Generator generator;
    auto tasks = generator.generate(test, Chooser::ChunkedGenerator());

    for(auto task : tasks)
    {
      std::cout << "--> " << task.get()->question.get_wording() << std::endl;
      std::cout << "[0] " << task.get()->answers[0].get_wording() << std::endl;
      std::cout << "[1] " << task.get()->answers[1].get_wording() << std::endl;
      std::cout << "[2] " << task.get()->answers[2].get_wording() << std::endl;
      std::cout << "[3] " << task.get()->answers[3].get_wording() << std::endl;
    }
  }

 private:
  JVocab::Test& test_choose(std::vector<JVocab::Test>& headers)
  {
    std::cout << "Choose a test:" << std::endl;
    for(size_t i=0; i<headers.size(); ++i)
    {
      TestHeader header = headers[i].get_header();
      std::cout << "[" << i << "] " << header.name << std::endl;
      std::cout << "  \t" << header.description << std::endl;
    }
    size_t chose;
    std::cin >> chose;
    
    if(chose >= headers.size())
      throw "incorrect test index";

    return headers[chose];
  }

};

}

#endif
