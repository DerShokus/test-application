#ifndef TEST_CHOOSE_HPP
#define TEST_CHOOSE_HPP

#include <vector>
#include <functional>
#include <cstdlib>
#include <ctime>
#include <memory>

#include "../include/test/Answer.hpp"
#include "../include/test/Question.hpp"

#include "JVocab/Test.hpp"

namespace Chooser
{
class Task 
{
 public:
  TextQuestion question;
  std::vector<TextAnswer> answers;

 public:
  Task(TextQuestion q, std::vector<TextAnswer>& answers) :
      question(q)
  {
      this->answers = answers;
  }
};

class ChunkedGenerator :
    public std::function<void(JVocab::Test&, size_t, std::vector<std::shared_ptr<Task>>&)>
{
 protected:
  const size_t repeates;
  const size_t chunked_items;
  const size_t answers_count;

 public:
  ChunkedGenerator(const size_t repeates_in_chunk = 8, 
                   const size_t items_in_chunk = 10,
                   const size_t answers = 4) 
      : repeates(repeates_in_chunk),
      chunked_items(items_in_chunk),
      answers_count(answers)
    {
    }

  void operator()(JVocab::Test& test,
                  size_t index,
                  std::vector<std::shared_ptr<Task>>& accumulator) 
  {
      std::srand(std::time(NULL));
      std::vector<Answer> answers;
      answers.reserve(answers_count);
      const Question question = test.question(index);
      const size_t start = index - index % chunked_items;

      for(size_t r=0; r<repeates; r++)
      {
          for(size_t i=0; i<answers_count; i++)
          {
              const size_t answer_index =  std::rand()% chunked_items + start;
              answers.push_back(test.answer(answer_index));
          }

          accumulator.push_back(std::shared_ptr<Task>(new Task(question, answers)));
      }
  }
};

class Generator 
{
 public:
  //typedef void(*TaskGeter)(Test&, size_t,std::vector<std::shared_ptr<Task>>&);
  typedef std::function<void(JVocab::Test&, size_t, std::vector<std::shared_ptr<Task>>&)> TaskGeter;
  std::vector<std::shared_ptr<Task>> generate(JVocab::Test& test, TaskGeter generator)
  {
      std::vector<std::shared_ptr<Task>> tasks;
      for(size_t i=0; i<test.size(); i++)
          generator(test, i, tasks);
      return tasks;
  }
};

}

#endif

