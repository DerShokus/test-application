#include "../../include/test/TestFactory.hpp"

#include "../JVocab/Test.hpp"

TestFactory::SharedTest TestFactory::get_header(const std::string& path)
{
  const size_t position = path.find_last_of(".");
  if(position == std::string::npos)
    return SharedTest(NULL);
  const std::string extention = path.substr(position + 1);

  std::fstream fs(path);
  if(extention == "csv")
  {
    auto header = new JVocab::Header;
    try
    {
      fs >> *header;
    }
    catch(std::string error)
    {
      delete header;
      throw;
    }
    return SharedTest(header);
  }

  return SharedTest(NULL);
}
