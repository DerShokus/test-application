#ifndef TEST_APP_SRC_JVOCAB_HEADER_H
#define TEST_APP_SRC_JVOCAB_HEADER_H

#include <iostream>
#include <vector>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>


#include "../../include/test/Header.hpp"

namespace JVocab
{

class Header : public TestHeader
{
 private:
  friend std::ostream& operator<<(std::ostream& out, const Header& header)
  {

    out << "#name=" << header.name << std::endl
        << "#description=" << header.description << std::endl;
    return out;
  }

  friend std::istream& operator>>(std::istream& in, Header& header)
  {
    std::pair<std::string, std::string> splited;
    std::string line;
    std::vector<std::string> pair;

    std::getline(in, line);
    boost::split(pair, line, boost::algorithm::is_any_of("="));
    splited.first = pair[0];
    splited.second = pair[1];

    if(splited.first.compare("#name") != 0)
      throw "bad format";
    header.name = splited.second;

    std::getline(in, line);
    pair.clear();
    boost::split(pair, line, boost::algorithm::is_any_of("="));
    splited.first = pair[0];
    splited.second = pair[1];
    if(splited.first.compare("#description") != 0)
      throw "bad format";
    header.description = splited.second;

    return in;
  }
};

}

#endif
