#ifndef TEST_APP_SRC_JVOCAB_ANSWER_H
#define TEST_APP_SRC_JVOCAB_ANSWER_H

#include <string>

#include "../../include/md5/md5.h"

namespace JVocab
{

class Answer : public ::Answer
{
 public:
  Answer(const std::string& str)
  {
    answer_hash = MD5::md5(str);
  }
};

}

#endif
