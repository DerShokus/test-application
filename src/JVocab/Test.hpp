#ifndef TEST_APP_JVOCAB_TEST_H 
#define TEST_APP_JVOCAB_TEST_H


#include <fstream>
#include <array>
#include <vector>
#include <utility>
#include <cstddef>
#include <exception>

#include "../../include/test/Test.hpp"

#include "Answer.hpp"
#include "Question.hpp"
#include "Header.hpp"
#include "Record.hpp"

namespace JVocab
{

class Test : public ::Test<TextQuestion, TextAnswer>
{
 private:
  Header header;

 public:
  bool correct(const std::shared_ptr<TextQuestion>& question, 
               const std::shared_ptr<TextAnswer>& answer) const
  {

    if(header.hash() == question.get()->test())
      throw "invalid question hash";

    auto found = answers.find(question);
    if(found == answers.end())
      return false;

    return found->second.get()->answer() == answer.get()->answer();

    throw "question not found";
  }

  size_t size() const 
  {
    return answers.size();
  }

  ::Question question(const size_t index) const
  {
    //return Question(records[index].first);
    return Question("error");
  }

  ::Answer answer(const size_t index) const
  {
    //return Answer(records[index].second);
    return Answer("error");
  }

  ::TestHeader get_header() const
  {
    return header;
  }

 private:
  friend std::ostream& operator<<(std::ostream &out, const Test &obj)
  {
    Record record; 

    out << obj.header;
    for (auto& r : obj.answers)
    {
      record.first = r.first.get()->get_wording();
      record.second = r.second.get()->get_wording();
      out << record;
    }
    return out;
  }

  friend std::istream& operator>>(std::istream &in, Test &obj)
  {
    in >> obj.header;

    Record record;
    while(!in.eof())
    {
      in >> record;
      if(!record.empty())
      {
        const auto key = std::make_shared<TextQuestion>(record.first);
        const auto value = std::make_shared<TextAnswer>(record.second);
        obj.answers[key] = value;
      }
    }

    return in;
  }
}; 

}

#endif
