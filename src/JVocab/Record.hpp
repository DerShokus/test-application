#ifndef TEST_APP_SRC_JVOCAB_RECORD_H
#define TEST_APP_SRC_JVOCAB_RECORD_H

#include <utility>
#include <string>
#include <iostream>
#include <vector>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>

namespace JVocab
{

class Record : public std::pair<std::string, std::string>
{
 public:

  void clear()
  {
    first.clear();
    second.clear();
  }

  Record& operator=(const std::pair<std::string, std::string>& record)
  {
    first = record.first;
    second = record.second;
    return *this;
  }

  bool empty() const
  {
    return first.empty() && second.empty();
  }

 private:
  friend std::ostream& operator<<(std::ostream& out, const Record& record)
  {
    out << record.first << ';' << record.second << std::endl;
    return out;
  }

  friend std::istream& operator>>(std::istream& in, Record& record)
  {
    record.clear();

    std::string line;
    std::getline(in, line);

    if(line.empty()) 
      return in;

    std::vector<std::string> result(2);

    if(boost::split(result, line, boost::algorithm::is_any_of(";")).empty())
      return in;

    record.first = result[0];
    record.second = result[1];
    return in;
  }
};

}

#endif
