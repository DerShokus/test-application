#ifndef TEST_APP_SRC_JVOCAB_QUESTION_H
#define TEST_APP_SRC_JVOCAB_QUESTION_H

#include <string>

#include "../../include/md5/md5.h"
#include "../../include/test/Question.hpp"


namespace JVocab
{

class Question : public ::Question
{
 public:
  Question(const std::string& str) 
  {
    question_hash = MD5::md5(str);
  }
};

}
#endif
